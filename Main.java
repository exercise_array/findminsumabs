import java.util.Arrays;

public class Main {

    public static void findMinSumAbs(int[] arr, int n) {
        if (n == 2) {
            System.out.println("Khong tim thay");
            return;
        }

        int e1 = -1, e2 = -1, minSum = Integer.MAX_VALUE;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (Math.abs(arr[i] + arr[j]) < minSum) {
                    minSum = Math.abs(arr[i] + arr[j]);
                    e1 = arr[i];
                    e2 = arr[j];
                }
            }
        }

        System.out.println("{" + e1 + "; " + e2  +"}");
    }

    public static void findMinSubAbsSort(int[] arr, int n) {
        if (n == 2) {
            System.out.println("Khong tim thay");
            return;
        }

        // Sắp xếp mảng
        Arrays.sort(arr);

        // Khởi tạo
        int e1 = 0, e2 = n-1, minSum = Integer.MAX_VALUE;
        // Lưu vị trí cặp số có trị tuyệt đối nhỏ nhất
        int min_e1 = 0, min_e2 = n-1;
        // Duyệt và tìm kiếm cặp số thoả điều kiện
        while (e1 < e2) {
            int sum = arr[e1] + arr[e2];
            if (Math.abs(sum) < Math.abs(minSum)) {
                minSum = sum;
                min_e1 = e1;
                min_e2 = e2;
            }
            if (sum < 0)
                e1++;
            else e2--;
        }
        // Xuất kết quả
        System.out.println("{" + arr[min_e1] + "; " + arr[min_e2]  +"}");
    }
    public static void main(String[] args) {
        int arr[] = {1, 60, -10, 70, -80, 85};
        findMinSubAbsSort(arr, arr.length);
    }
}
